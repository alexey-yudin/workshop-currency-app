export class CurrencyApi {
  _baseUrl = "https://www.alphavantage.co";
  _apiKey = '451UKZQUSQQZ60IU';

  fetchDailyPrices(currency, toCurrency) {
    const apiUrl = `${this._baseUrl}/query?function=DIGITAL_CURRENCY_DAILY&symbol=${currency}&market=${toCurrency}&apikey=${this._apiKey}`;

    return fetch(apiUrl).then(response => response.json());
  }

  exchange(currencyFrom, currencyTo) {
    const apiUrl = `${this._baseUrl}/query?function=CURRENCY_EXCHANGE_RATE&from_currency=${currencyFrom}&to_currency=${currencyTo}&apikey=${this._apiKey}`

    return fetch(apiUrl).then(response => response.json());
  }
}
