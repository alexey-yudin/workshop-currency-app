import React from 'react';
import Container from '@material-ui/core/Container';
import Toolbar from '@material-ui/core/Toolbar';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';

import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import {CurrencyChart} from './components/currency-chart/currency-chart';
import {CurrencyExchange} from "./components/currency-exchange/currency-exchange";

function App() {
  return (
    <>
      <AppBar position="static">
        <Toolbar>
          <Typography>
            Currency app
          </Typography>
        </Toolbar>
      </AppBar>
      <Router>
        <Container maxWidth="lg">
          <Switch>
            <Route exact path="/">
              <CurrencyChart />
            </Route>
            <Route path="/exchange">
              <CurrencyExchange/>
            </Route>
          </Switch>
        </Container>
      </Router>
    </>
  );
}

export default App;
