import React, {useEffect} from 'react';
import {CurrencyApi} from '../../api/currency-api'
import {createChart} from "../../services/currency-service";

export function CurrencyChart() {
  useEffect(() => {
    const currencyApi = new CurrencyApi();
    const currencyFrom = 'BTC';
    const currencyTo = 'USD';
    currencyApi.fetchDailyPrices(currencyFrom, currencyTo)
      .then(prices => {
        const parsedPrices = prices['Time Series (Digital Currency Daily)'];
        const result = Object.keys(parsedPrices)
          .map(key => {
            const date = new Date(key).getTime();
            const price = parseFloat(parsedPrices[key]['1a. open (USD)']);
            return [date, price];
          });
        createChart(currencyFrom, currencyTo, result);
      });
  });

  return (
    <div>
      <div id="chart-container"/>
    </div>
  );
}
