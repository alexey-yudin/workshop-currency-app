import React, {useState} from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import './currency-exchange.css';
import {CurrencyApi} from "../../api/currency-api";
import {ExchangeService} from "../../services/exchange-service";

export function CurrencyExchange(props) {
  const [amount, setAmount] = useState("");
  const [currencyFrom, setCurrencyFrom] = useState('BTC');
  const [currencyTo, setCurrencyTo] = useState('USD');
  const [result, setResult] = useState("");

  const handleAmountChange = event => setAmount(event.target.value);
  const handleCurrencyFromChange = event => setCurrencyFrom(event.target.value);
  const handleCurrencyToChange = event => setCurrencyTo(event.target.value);
  const api = new CurrencyApi();
  const exchangeService = new ExchangeService();

  const onExchange = () => {
    api.exchange(currencyFrom, currencyTo)
      .then(exchangeRate => {
        // TODO: write your own api
        exchangeRate = 4;
        const result = exchangeService.calculateExchange(currencyFrom, currencyTo, exchangeRate);
        setResult(result);
      });
  };

  return (
    <div className="container">
      <form className="form-fields">
        <TextField
          className="form-field__item"
          type="number"
          label="Amount"
          value={amount}
          onChange={handleAmountChange}
        />

        <Select
          native
          label="From"
          value={currencyFrom}
          onChange={handleCurrencyFromChange}>
          <option value={'USD'}>USD</option>
          <option value={'BTC'}>BTC</option>
          <option value={'PLN'}>PLN</option>
        </Select>

        <Select
          native
          label="From"
          value={currencyTo}
          onChange={handleCurrencyToChange}>
          <option value={'USD'}>USD</option>
          <option value={'BTC'}>BTC</option>
          <option value={'PLN'}>PLN</option>
        </Select>

        <Button variant="contained" onClick={onExchange()}>Exchange</Button>
      </form>

      <div className="result">
        {result}
      </div>
    </div>
  );
}
