import * as Highcharts from 'highcharts';

export function createChart(currencyFrom, currencyTo, data) {
  Highcharts.chart('chart-container', {
    title: {
      text: `${currencyFrom} to ${currencyTo} exchange rate`
    },
    yAxis: {
      title: {
        text: 'Exchange rate'
      }
    },
    xAxis: {
      title: {
        text: 'Date'
      },
      type: 'datetime'
    },
    series: [{
      type: 'area',
      name: `${currencyFrom} to ${currencyTo}`,
      data
    }]
  });
}
